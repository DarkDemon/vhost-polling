obj-$(CONFIG_I40E) += i40e.o

i40e-objs := i40e_main.o \
	i40e_ethtool.o	\
	i40e_adminq.o	\
	i40e_common.o	\
	i40e_hmc.o	\
	i40e_lan_hmc.o	\
	i40e_nvm.o	\
	i40e_debugfs.o	\
	i40e_diag.o	\
	i40e_txrx.o	\
	i40e_ptp.o	\
	i40e_client.o   \
	i40e_virtchnl_pf.o \
	i40e_poll.o

i40e-$(CONFIG_I40E_DCB) += i40e_dcb.o i40e_dcb_nl.o
i40e-$(CONFIG_I40E_FCOE) += i40e_fcoe.o

obj-$(CONFIG_VHOST_NET) += vhost_net.o vhost_polling.o
vhost_net-y := net.o
obj-$(CONFIG_VHOST)	+= vhost.o vhost_polling.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(shell pwd)	
clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(shell pwd) clean
