#include <linux/netdevice.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/irq.h>

#include "i40e.h"

int i40e_enable_poll(struct net_device * netdev)
{
	struct i40e_netdev_priv *np = netdev_priv(netdev);
	struct i40e_vsi *vsi;
	struct i40e_pf *pf;
	struct i40e_hw *hw;
	int vector/*, q*/;
	u32 qp;

	if (np->magic != I40E_NETDEV_PRIV_MAGIC)
		return 0;

	vsi = np->vsi;
	pf = vsi->back;
	hw = &pf->hw;
	qp = vsi->base_queue;

	wr32(hw, I40E_GLINT_CTL, I40E_GLINT_CTL_DIS_AUTOMASK_N_MASK); // Disable automatic interrupt disable
	for (vector = 0; vector < vsi->num_q_vectors; vector++)
	{
		/*for (q = 0; q < vsi->q_vectors[vector]->num_ringpairs; q++) 
		{
			wr32(hw, I40E_QINT_TQCTL(qp), rd32(hw, I40E_QINT_TQCTL(qp)) & ~I40E_QINT_TQCTL_CAUSE_ENA_MASK);
			qp++;
		}*/
		disable_irq(pf->msix_entries[vsi->base_vector + vector].vector);
		napi_synchronize(&vsi->q_vectors[vector]->napi);
	}
	
	i40e_flush(hw);

	return vsi->num_q_vectors;
}
EXPORT_SYMBOL_GPL(i40e_enable_poll);

int i40e_poll(struct net_device * netdev, int vector, int budget)
{
	struct i40e_netdev_priv *np = netdev_priv(netdev);
	struct i40e_vsi *vsi = np->vsi;
	struct i40e_q_vector * qvec = vsi->q_vectors[vector];
	return i40e_poll_vector(qvec, budget);
}
EXPORT_SYMBOL_GPL(i40e_poll);

void i40e_disable_poll(struct net_device * netdev)
{
	struct i40e_netdev_priv *np = netdev_priv(netdev);
	struct i40e_vsi *vsi = np->vsi;
	struct i40e_pf *pf = vsi->back;
	struct i40e_hw *hw = &pf->hw;
	int vector/*, q*/;
	u32 qp = vsi->base_queue;

	wr32(hw, I40E_GLINT_CTL, 0);
	for (vector = 0; vector < vsi->num_q_vectors; vector++)
	{
		u32 val = I40E_PFINT_DYN_CTLN_INTENA_MASK |
  	  			  I40E_PFINT_DYN_CTLN_ITR_INDX_MASK | /* set noitr */
  				  I40E_PFINT_DYN_CTLN_SWINT_TRIG_MASK |
 	 			  I40E_PFINT_DYN_CTLN_SW_ITR_INDX_ENA_MASK;

		/*for (q = 0; q < vsi->q_vectors[vector]->num_ringpairs; q++) 
		{
			wr32(hw, I40E_QINT_TQCTL(qp), rd32(hw, I40E_QINT_TQCTL(qp)) | I40E_QINT_TQCTL_CAUSE_ENA_MASK);
			qp++;
		}*/

		enable_irq(pf->msix_entries[vsi->base_vector + vector].vector);

		wr32(hw, I40E_PFINT_DYN_CTLN(vector + vsi->base_vector - 1), val); // Trigger interrupt to cleanup everything
	}
	i40e_flush(hw);
}
EXPORT_SYMBOL_GPL(i40e_disable_poll);
