#include <linux/eventfd.h>
#include <linux/vhost.h>
#include <linux/mm.h>
#include <linux/mutex.h>
#include <linux/poll.h>
#include <linux/file.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/slab.h>
#include <linux/vmalloc.h>
#include <linux/kthread.h>
#include <linux/mmu_context.h>
#include <linux/net.h>
#include <linux/netpoll.h>
#include <linux/rtnetlink.h>
#include <linux/cpumask.h>
#include <linux/sched.h>
#include "vhost.h"
#include "vhost_polling.h"
#include "vhost_polling_user.h"
#ifdef I40E_POLL
#include "i40e_poll.h"
#endif

#define VHOST_NET_WEIGHT 0x80000
#define VHOST_POLLING_DEFAULT_MIN_IOPS 10
#define VHOST_POLLING_DEFAULT_MAX_IOPS 500
#define VHOST_POLLING_DEFAULT_MAX_STUCK_CYCLES (1 << 14)
#define SHARED_OWNER (struct vhost_pollworker *)(-1)

#ifdef I40E_POLL
#define IFACE_POLL_RATE (1 << 22)
#define IFACE_POLL_BUDGET 300
#endif

struct
{
    struct mutex mutex;

    u64 next_devid;
    struct list_head closed_vqs;

    struct list_head shared_vqs;
#ifdef I40E_POLL
    struct list_head shared_ifaces;
    u64 iface_clock;
#endif
} polling_state;

static void * vhost_polling_kvmalloc(unsigned long size)
{
	void * n = kmalloc(size, GFP_KERNEL | __GFP_NOWARN | __GFP_REPEAT);
	if (n)
		return n;
	return vmalloc(size);
}

static void vhost_polling_set_mm(struct mm_struct * mm)
{
    if (current->mm != mm)
        use_mm(mm);
}

static bool vhost_polling_poll_queue_callback(void * param)
{
    struct vhost_pollvq * pvq = param;
    if (!pvq->owner)
        return false;
    atomic_set(&pvq->poll_wakeup, true);
    return true;
}

static bool vhost_polling_poll_flush_callback(void * param)
{
    struct vhost_pollvq * pvq = param;
    return pvq->owner != NULL;
}

static bool vhost_polling_trylock_vq(struct vhost_pollvq * pvq, struct vhost_pollworker * worker)
{
    if (!mutex_trylock(&pvq->lock))
        return false;
    pvq->locker = worker;
    return true;
}

static void vhost_polling_unlock_vq(struct vhost_pollvq * pvq)
{
    pvq->locker = NULL;
    mutex_unlock(&pvq->lock);
}

static bool vhost_polling_should_kick_vq(struct vhost_pollvq * pvq)
{
    return atomic_read(&pvq->poll_wakeup) || (pvq->avail_mapped->idx != pvq->stuck_avail_idx && pvq->avail_mapped->idx != pvq->vq.last_avail_idx);
}

static void vhost_polling_kick_vq(struct vhost_pollvq * pvq)
{
    u64 time;
    u16 last_avail_idx = pvq->vq.last_avail_idx;
    u16 avail_idx = pvq->avail_mapped->idx;

    vhost_polling_set_mm(pvq->vq.dev->mm);
    atomic_set(&pvq->poll_wakeup, false);

    rdtscll(time);
    pvq->vq.poll.work.fn(&pvq->vq.poll.work);
    rdtscll(pvq->stats.last_work);

    pvq->stats.work_time += pvq->stats.last_work - time;

    if (pvq->vq.last_avail_idx == last_avail_idx)
        pvq->stuck_avail_idx = avail_idx;
    else
        pvq->stuck_avail_idx = -1;
}

static void vhost_polling_roundrobin(struct vhost_pollworker * worker)
{
    struct vhost_pollvq * pvq;
    u64 limited_count;
    
    if (worker->stuck_pvq)
    {
        vhost_polling_kick_vq(worker->stuck_pvq);
        vhost_polling_unlock_vq(worker->stuck_pvq);
        worker->stuck_pvq = NULL;
        vhost_polling_kick_vq(worker->last_pvq);
        return;
    }

    pvq = worker->last_pvq ? worker->last_pvq : container_of(worker->vq_list, struct vhost_pollvq, vq_node);
    rcu_read_lock();
    list_for_each_entry_continue_rcu(pvq, worker->vq_list, vq_node)
    {
        if (!vhost_polling_trylock_vq(pvq, worker))
            continue;

        if (vhost_polling_should_kick_vq(pvq))
        {
            if (worker->last_pvq)
                vhost_polling_unlock_vq(worker->last_pvq);
            worker->last_pvq = pvq;
            rcu_read_unlock();
            limited_count = pvq->stats.limited_count;
            vhost_polling_kick_vq(pvq);
            return;
        }

        vhost_polling_unlock_vq(pvq);
    }
    if (worker->last_pvq)
        vhost_polling_unlock_vq(worker->last_pvq);
    worker->last_pvq = NULL;
    rcu_read_unlock();
}

static void vhost_polling_unlock_worker(struct vhost_pollworker * worker)
{
    if (worker->stuck_pvq)
    {
        vhost_polling_unlock_vq(worker->stuck_pvq);
        worker->stuck_pvq = NULL;
    }
    if (worker->last_pvq)
    {
        vhost_polling_unlock_vq(worker->last_pvq);
        worker->last_pvq = NULL;
    }
}


#ifdef I40E_POLL
static void vhost_polling_i40e_poll(struct vhost_pollworker * worker, int budget)
{
    struct vhost_polliface * iface;
    rcu_read_lock();
    list_for_each_entry_rcu(iface, worker->iface_list, iface_node)
    {
        if (!mutex_trylock(&iface->lock))
            continue;
        rcu_read_unlock();
        budget -= i40e_poll(iface->netdev, iface->vector, budget);
        rcu_read_lock();
        mutex_unlock(&iface->lock);
        if (budget <= 0)
            break;
    }
    rcu_read_unlock();
}
#endif

static int vhost_polling_worker(void *data)
{
    struct vhost_pollworker * worker = data;
    struct mm_struct * mm = current->mm;
    mm_segment_t oldfs = get_fs();
#ifdef I40E_POLL
    u64 cycles, last_cycles;
#endif

    set_fs(USER_DS);

    for (;;)
    {
        if (kthread_should_stop())
            break;

        vhost_polling_roundrobin(worker);

#ifdef I40E_POLL
        rdtscll(cycles);
        last_cycles = READ_ONCE(polling_state.iface_clock);
        if (cycles - last_cycles > IFACE_POLL_RATE &&
            last_cycles == cmpxchg(&polling_state.iface_clock, last_cycles, cycles))
        {
            vhost_polling_i40e_poll(worker, IFACE_POLL_BUDGET);
        }
#endif
        if (unlikely(worker->should_sleep))
        {
            vhost_polling_unlock_worker(worker);
            set_current_state(TASK_INTERRUPTIBLE);
            schedule();
            __set_current_state(TASK_RUNNING);
        }
        
        if (need_resched())
            schedule();
    }

    vhost_polling_unlock_worker(worker);
    set_fs(oldfs);
    if (current->mm != mm)
        unuse_mm(current->mm);
    return 0;
}

static void vhost_polling_vq_detach_owner(struct vhost_pollvq * pvq, struct socket * sock)
{
    struct mm_struct * mm;

    pvq->stuck_avail_idx = -1;
    
    mutex_lock(&pvq->vq.mutex);
    vhost_poll_stop(pvq->real_poll);
    pvq->vq.private_data = NULL;
    vhost_poll_stop(&pvq->vq.poll);
    mutex_unlock(&pvq->vq.mutex);

    vhost_poll_flush(pvq->real_poll);
    vhost_poll_flush(&pvq->vq.poll);

    if (pvq->owner)
    {
        list_del_rcu(&pvq->vq_node);
        synchronize_rcu();

        mutex_lock(&pvq->lock);
        mutex_unlock(&pvq->lock);

        atomic_set(&pvq->poll_wakeup, false);
        if (pvq->owner != SHARED_OWNER)
            fput(pvq->owner->file);
    }
    else 
    {
        vhost_disable_notify(pvq->vq.dev, &pvq->vq);

        mm = current->mm;
        vhost_polling_set_mm(pvq->vq.dev->mm);
        __get_user(pvq->vq.avail_idx, &pvq->vq.avail->idx);
        BUG_ON(get_user_pages_fast((unsigned long)pvq->vq.avail, 1, 0,
                &pvq->avail_page) != 1);
        pvq->avail_mapped = (struct vring_avail *)(
        (unsigned long)kmap(pvq->avail_page) |
        ((unsigned long)pvq->vq.avail & ~PAGE_MASK));
        vhost_polling_set_mm(mm);
    }
}

static void vhost_polling_vq_attach_owner(struct vhost_pollvq * pvq, struct socket * sock)
{
    if (pvq->owner)
    {
        pvq->vq.poll.queue_callback = pvq->real_poll->queue_callback = vhost_polling_poll_queue_callback;
        pvq->vq.poll.flush_callback = pvq->real_poll->flush_callback = vhost_polling_poll_flush_callback;
        pvq->vq.poll.callback_param = pvq->real_poll->callback_param = pvq;

        if (pvq->owner == SHARED_OWNER)
            pvq->vq_list = &polling_state.shared_vqs;
        else
            pvq->vq_list = pvq->owner->vq_list;

        smp_wmb();
        list_add_tail_rcu(&pvq->vq_node, pvq->vq_list);
    }
    else
    {   
        pvq->vq.poll.queue_callback = pvq->real_poll->queue_callback = NULL;
        pvq->vq.poll.flush_callback = pvq->real_poll->flush_callback = NULL;
        pvq->vq.poll.callback_param = pvq->real_poll->callback_param = NULL;

        kunmap(pvq->avail_page);
        put_page(pvq->avail_page);
        pvq->avail_mapped = NULL;
        pvq->avail_page = NULL;

        if (vhost_enable_notify(pvq->vq.dev, &pvq->vq))
            vhost_poll_queue(&pvq->vq.poll);
    }

    mutex_lock(&pvq->vq.mutex);
    pvq->vq.private_data = sock;
    vhost_poll_start(pvq->real_poll, sock->file);
    vhost_poll_start(&pvq->vq.poll, pvq->vq.kick);
    mutex_unlock(&pvq->vq.mutex);
}

static void vhost_polling_vq_set_owner(struct vhost_pollvq * pvq, struct vhost_pollworker * owner)
{
    struct socket * sock = pvq->vq.private_data;

    if (pvq->owner == owner)
        return;

    vhost_polling_vq_detach_owner(pvq, sock);
    pvq->owner = owner;
    vhost_polling_vq_attach_owner(pvq, sock);
}

void vhost_polling_dev_init(struct vhost_dev * dev, struct vhost_poll * polls)
{
    int i = 0;
    struct vhost_pollvq * vq = NULL;
    u64 devid = 0;

    mutex_lock(&polling_state.mutex);

    devid = polling_state.next_devid++;

    for (i = 0; i < dev->nvqs; i++)
    {
        vq = container_of(dev->vqs[i], struct vhost_pollvq, vq);

        vq->file = NULL;

        list_add(&vq->closed_node, &polling_state.closed_vqs);
        INIT_LIST_HEAD(&vq->vq_node);

        vq->owner = NULL;
        vq->locker = NULL;
        mutex_init(&vq->lock);

        vq->avail_mapped = NULL;
        vq->avail_page = NULL;

        vq->real_poll = &polls[i];
        atomic_set(&vq->poll_wakeup, false);

        vq->min_iops = VHOST_POLLING_DEFAULT_MIN_IOPS;
        vq->max_iops = VHOST_POLLING_DEFAULT_MAX_IOPS;
        vq->cur_iops = 0;

        vq->max_stuck_cycles = VHOST_POLLING_DEFAULT_MAX_STUCK_CYCLES;
        vq->stuck_avail_idx = -1;

        memset(&vq->stats, 0, sizeof(vq->stats));
        vq->stats.devid = devid;
        vq->stats.vqid = i;
    }

    mutex_unlock(&polling_state.mutex);
}
EXPORT_SYMBOL_GPL(vhost_polling_dev_init);

void vhost_polling_dev_stop(struct vhost_dev * dev)
{
    int i = 0;
    struct vhost_pollvq * vq = NULL;
    
    mutex_lock(&polling_state.mutex);

    for (i = 0; i < dev->nvqs; i++)
    {
        vq = container_of(dev->vqs[i], struct vhost_pollvq, vq);

        if (vq->file)
            vq->file->private_data = NULL;
        else
            list_del_init(&vq->closed_node);

        vhost_polling_vq_set_owner(vq, NULL);
    }

    mutex_unlock(&polling_state.mutex);
}
EXPORT_SYMBOL_GPL(vhost_polling_dev_stop);

int vhost_polling_enable_notify(struct vhost_dev * dev, struct vhost_virtqueue * vq)
{
    struct vhost_pollvq * pvq = container_of(vq, struct vhost_pollvq, vq);
    if (pvq->owner)
        return 0;
    return vhost_enable_notify(dev, vq);
}
EXPORT_SYMBOL_GPL(vhost_polling_enable_notify);

bool vhost_polling_should_stop(struct vhost_dev * dev, struct vhost_virtqueue * vq, size_t total_len)
{
    struct vhost_pollvq * pvq = container_of(vq, struct vhost_pollvq, vq);
    struct vhost_pollvq * entry;
    u64 cycles;

    if (!pvq->locker)
        return total_len >= VHOST_NET_WEIGHT;

    if (pvq->locker->stuck_pvq)
        return true;

    pvq->cur_iops++;
    if (pvq->cur_iops < pvq->min_iops)
        return false;
    if (pvq->cur_iops > pvq->max_iops)
        goto should_stop;

    rdtscll(cycles);

    rcu_read_lock();

    list_for_each_entry_rcu(entry, &pvq->vq_node, vq_node)
    {
        if (&entry->vq_node == pvq->vq_list)
            continue;

        if (!vhost_polling_trylock_vq(entry, pvq->locker))
            continue;

        if ((entry->max_stuck_cycles < 0) || 
            (!vhost_polling_should_kick_vq(entry)) || 
            ((cycles - entry->stats.last_work) < entry->max_stuck_cycles))
        {
            vhost_polling_unlock_vq(entry);
            continue;
        }
        
        pvq->locker->stuck_pvq = entry;
        entry->stats.stuck_count++;
        entry->stats.stuck_time += cycles - pvq->stats.last_work;
        rcu_read_unlock();
        goto should_stop;
    }
    rcu_read_unlock();
    return false;

should_stop:
    pvq->stats.limited_count++;
    pvq->cur_iops = 0;
    return true;
}
EXPORT_SYMBOL_GPL(vhost_polling_should_stop);

void vhost_polling_handle_vq_callback(struct vhost_virtqueue * vq)
{
    struct vhost_pollvq * pvq = container_of(vq, struct vhost_pollvq, vq);
    if (!pvq->owner)
        pvq->stats.work_count++;
}
EXPORT_SYMBOL_GPL(vhost_polling_handle_vq_callback);

static int vhost_polling_pollworker_open(struct inode *inode, struct file *f)
{
    struct vhost_pollworker * worker;

    worker = vhost_polling_kvmalloc(sizeof(*worker));
    if (!worker)
        return -ENOMEM;

    worker->thread = kthread_create(vhost_polling_worker, worker, "vhost-polling-worker");
    if (IS_ERR(worker->thread)) 
    {
        kvfree(worker);
        return PTR_ERR(worker->thread);
    }

    worker->started = false;
    worker->should_sleep = true;
    INIT_LIST_HEAD(&worker->vq_head);
    worker->vq_list = &worker->vq_head;
#ifdef I40E_POLL
    worker->iface_list = &polling_state.shared_ifaces;
#endif
    worker->last_pvq = NULL;
    worker->stuck_pvq = NULL;

    worker->file = f;
    f->private_data = worker;

    return 0;
}

static int vhost_polling_pollworker_release(struct inode *inode, struct file *f)
{
    struct vhost_pollworker * worker;
    worker = f->private_data;

    kthread_stop(worker->thread);
    kvfree(worker);
    return 0;
}
#ifdef I40E_POLL
static long vhost_polling_iface_poll_enable(struct vhost_pollworker * worker, const char __user * name)
{
    char if_name[IFNAMSIZ];
    struct net *net;
    struct net_device * netdev;
    int vcnt, i;
    struct vhost_polliface * iface;
    struct vhost_polliface * ifaces;

    unsigned long size = copy_from_user(if_name, name, IFNAMSIZ);
    if (!memchr(if_name, 0, IFNAMSIZ - size))
        return -EFAULT;

    rtnl_lock();        
    net = current->nsproxy->net_ns;
    netdev = __dev_get_by_name(net, if_name);
    rtnl_unlock();

    if (!netdev)
        return -EINVAL;

    list_for_each_entry_rcu(iface, worker->iface_list, iface_node)
    {
        if (iface->netdev == netdev)
            return -EBUSY;
    }

    if (!(vcnt = i40e_enable_poll(netdev)))
        return -EINVAL;

    ifaces = vhost_polling_kvmalloc(sizeof(struct vhost_polliface) * vcnt);
    if (!ifaces)
    {
        i40e_disable_poll(netdev);
        return -ENOMEM;
    }

    for (i = 0; i < vcnt; i++)
    {
        ifaces[i].netdev = netdev;
        ifaces[i].vector = i;
        mutex_init(&ifaces[i].lock);
        smp_wmb();
        list_add_tail_rcu(&ifaces[i].iface_node, worker->iface_list);
    }

    return 0;
}

static long vhost_polling_iface_poll_disable(struct vhost_pollworker * worker, const char __user * name)
{
    char if_name[IFNAMSIZ];
    struct net *net;
    struct net_device * netdev;
    struct vhost_polliface * iface, * n;
    struct vhost_polliface * ifaces = NULL;

    unsigned long size = copy_from_user(if_name, name, IFNAMSIZ);
    if (!memchr(if_name, 0, IFNAMSIZ - size))
        return -EFAULT;

    rtnl_lock();        
    net = current->nsproxy->net_ns;
    netdev = __dev_get_by_name(net, if_name);
    rtnl_unlock();

    if (!netdev)
        return -EINVAL;

    list_for_each_entry_safe(iface, n, worker->iface_list, iface_node)
    {
        if (iface->netdev == netdev)
        {
            if (iface->vector == 0)
                ifaces = iface;
            list_del_rcu(&iface->iface_node);
            synchronize_rcu();
            mutex_lock(&iface->lock);
            mutex_unlock(&iface->lock);
        }
    }

    kvfree(ifaces);
    i40e_disable_poll(netdev);

    return 0;
}
#endif

static long vhost_polling_pollworker_ioctl(struct file *f, unsigned int ioctl,
                unsigned long arg)
{
    struct vhost_pollworker * worker = f->private_data;
    long r;
    static cpumask_t mask = CPU_MASK_NONE;

    mutex_lock(&polling_state.mutex);
    switch(ioctl)
    {        
    case VHOST_POLLING_POLLWORKER_ENABLE_SHARED:
        if (!list_empty(worker->vq_list) || worker->started)
        {
            r = -EBUSY;
            break;
        }
        worker->vq_list = &polling_state.shared_vqs;
        r = 0;
        break;
    case VHOST_POLLING_POLLWORKER_RESUME:
        worker->started = true;
        worker->should_sleep = false;
        wake_up_process(worker->thread);
        r = 0;
        break;      
    case VHOST_POLLING_POLLWORKER_SUSPEND:
        worker->should_sleep = true;
        r = 0;
        break;
#ifdef I40E_POLL
    case VHOST_POLLING_POLLWORKER_START_POLL_IFACE:
        r = vhost_polling_iface_poll_enable(worker, (const char __user *)arg);
        break;
    case VHOST_POLLING_POLLWORKER_STOP_POLL_IFACE:
        r = vhost_polling_iface_poll_disable(worker, (const char __user *)arg);
        break;
#endif
    case VHOST_POLLING_POLLWORKER_BIND:
        cpumask_clear(&mask);
        cpumask_set_cpu(arg, &mask);
        r = set_cpus_allowed_ptr(worker->thread, &mask);
        break;
    default:
        r = -ENOIOCTLCMD;
        break;
    }

    mutex_unlock(&polling_state.mutex);
    return r;
}

static const struct file_operations vhost_polling_pollworker_fops =
{
    .owner = THIS_MODULE,
    .open = vhost_polling_pollworker_open,
    .release = vhost_polling_pollworker_release,
    .unlocked_ioctl = vhost_polling_pollworker_ioctl
};

static struct miscdevice vhost_polling_pollworker_misc =
{
    .minor = MISC_DYNAMIC_MINOR,
    .name = "vhost-polling-worker",
    .fops = &vhost_polling_pollworker_fops,
    .mode = 0500,
};

static int vhost_polling_pollvq_open(struct inode *inode, struct file *f)
{
    struct vhost_pollvq * vq;
    mutex_lock(&polling_state.mutex);
    vq = list_first_entry_or_null(&polling_state.closed_vqs, struct vhost_pollvq, closed_node);
    if (!vq)
    {
        mutex_unlock(&polling_state.mutex);
        return -ENODEV;
    }
    list_del_init(&vq->closed_node);
    f->private_data = vq;
    vq->file = f;
    mutex_unlock(&polling_state.mutex);
    return 0;
}

static int vhost_polling_pollvq_release(struct inode *inode, struct file *f)
{
    struct vhost_pollvq * vq = f->private_data;
    mutex_lock(&polling_state.mutex);
    if (vq)
    {
        list_add(&vq->closed_node, &polling_state.closed_vqs);
        vq->file = NULL;
        vhost_polling_vq_set_owner(vq, NULL);
    }
    mutex_unlock(&polling_state.mutex);
    return 0;
}

static long vhost_polling_pollvq_ioctl(struct file *f, unsigned int ioctl,
                unsigned long arg)
{
    struct vhost_pollvq * vq = f->private_data;
    struct file * worker_file;
    struct vhost_pollworker * worker;
    long r;

    mutex_lock(&polling_state.mutex);
    if (!vq || !vq->vq.avail || !vq->vq.private_data)
    {
        mutex_unlock(&polling_state.mutex);
        return -EBADFD;
    }
    switch(ioctl)
    {        
    case VHOST_POLLING_POLLVQ_SET_OWNER:
        worker_file = arg != (int)arg ? NULL : fget(arg);
        if (!worker_file)
        {
            r = -EBADF;
            break;
        }
        if (imajor(worker_file->f_path.dentry->d_inode) != MISC_MAJOR || 
            iminor(worker_file->f_path.dentry->d_inode) != vhost_polling_pollworker_misc.minor)
        {
            fput(worker_file);
            r = -EINVAL;
            break;
        }
        worker = worker_file->private_data;
        vhost_polling_vq_set_owner(vq, worker);
        r = 0;
        break;
    case VHOST_POLLING_POLLVQ_UNSET_OWNER:
        vhost_polling_vq_set_owner(vq, NULL);
        r = 0;
        break;
    case VHOST_POLLING_POLLVQ_ENABLE_SHARED:
        vhost_polling_vq_set_owner(vq, SHARED_OWNER);
        r = 0;
        break;
    case VHOST_POLLING_POLLVQ_GET_STATS:
        rdtscll(vq->stats.tsc);
        if (copy_to_user((void __user *)arg, &vq->stats, sizeof(vq->stats)))
            r = -EFAULT;
        else
            r = 0;
        break;
    default:
        r = -ENOIOCTLCMD;
    }

    mutex_unlock(&polling_state.mutex);
    return r;
}

static const struct file_operations vhost_polling_pollvq_fops =
{
    .owner = THIS_MODULE,
    .open = vhost_polling_pollvq_open,
    .release = vhost_polling_pollvq_release,
    .unlocked_ioctl = vhost_polling_pollvq_ioctl,
};

static struct miscdevice vhost_polling_pollvq_misc =
{
    .minor = MISC_DYNAMIC_MINOR,
    .name = "vhost-polling-vq",
    .fops = &vhost_polling_pollvq_fops,
    .mode = 0500,
};

static int __init vhost_polling_init(void)
{
    int ret;

    mutex_init(&polling_state.mutex);
    polling_state.next_devid = 0;
    INIT_LIST_HEAD(&polling_state.closed_vqs);
    INIT_LIST_HEAD(&polling_state.shared_vqs);
#ifdef I40E_POLL
    INIT_LIST_HEAD(&polling_state.shared_ifaces);
    rdtscll(polling_state.iface_clock);
#endif

    if ((ret = misc_register(&vhost_polling_pollworker_misc)))
        return ret;
    return misc_register(&vhost_polling_pollvq_misc);
}
module_init(vhost_polling_init);

static void __exit vhost_polling_exit(void)
{
    misc_deregister(&vhost_polling_pollworker_misc);
    misc_deregister(&vhost_polling_pollvq_misc);
}
module_exit(vhost_polling_exit);

MODULE_LICENSE("GPL v2");
