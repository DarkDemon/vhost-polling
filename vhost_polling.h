#ifndef _VHOST_POLLING_H
#define _VHOST_POLLING_H

#include "vhost.h"
#include "vhost_polling_user.h"
#include <linux/miscdevice.h>
#include <linux/netdevice.h>

#define VHOST_POLLING

struct vhost_pollworker
{
	struct task_struct * thread;
	struct file * file;

	bool started;
	bool should_sleep;

	struct list_head vq_head;
	struct list_head * vq_list;
#ifdef I40E_POLL
	struct list_head * iface_list;
#endif

	struct vhost_pollvq * last_pvq;
	struct vhost_pollvq * stuck_pvq;
};

struct vhost_pollvq
{
	struct vhost_virtqueue vq;

	struct file * file;
	
	struct list_head closed_node;
	struct list_head vq_node;
	struct list_head * vq_list;

	struct vhost_pollworker * owner;
	struct vhost_pollworker * locker;

	struct mutex lock;

	volatile struct vring_avail *avail_mapped;
	struct page * avail_page;

	struct vhost_poll * real_poll;
	atomic_t poll_wakeup;

	size_t min_iops;
	size_t max_iops;
	size_t cur_iops;

	u64 max_stuck_cycles;

	s32 stuck_avail_idx;

	struct vhost_pollvq_stats stats;
};

#ifdef I40E_POLL
struct vhost_polliface
{
	struct net_device * netdev;
	int vector;
	struct mutex lock;
	struct list_head iface_node;
};
#endif

extern void vhost_polling_dev_init(struct vhost_dev * dev, struct vhost_poll * polls);
extern void vhost_polling_dev_stop(struct vhost_dev * dev);

extern void vhost_polling_vq_poll_stop(struct vhost_virtqueue * vq);
extern int vhost_polling_vq_poll_start(struct vhost_virtqueue * vq);
extern int vhost_polling_enable_notify(struct vhost_dev * dev, struct vhost_virtqueue * vq);

extern bool vhost_polling_should_stop(struct vhost_dev * dev, struct vhost_virtqueue * vq, size_t total_len);
extern void vhost_polling_handle_vq_callback(struct vhost_virtqueue * vq);

#endif
