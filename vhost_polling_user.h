#include <asm/ioctl.h>
#include <linux/netdevice.h>

#ifndef _VHOST_POLLING_USER_H
#define _VHOST_POLLING_USER_H

typedef char if_name_t[IFNAMSIZ];

#define VHOST_POLLING_POLLWORKER_VIRTIO 0xC8
#define VHOST_POLLING_POLLWORKER_ENABLE_SHARED _IO(VHOST_POLLING_POLLWORKER_VIRTIO, 0x01)
#define VHOST_POLLING_POLLWORKER_RESUME _IO(VHOST_POLLING_POLLWORKER_VIRTIO, 0x02)
#define VHOST_POLLING_POLLWORKER_SUSPEND _IO(VHOST_POLLING_POLLWORKER_VIRTIO, 0x03)
#define VHOST_POLLING_POLLWORKER_START_POLL_IFACE _IOR(VHOST_POLLING_POLLWORKER_VIRTIO, 0x04, if_name_t)
#define VHOST_POLLING_POLLWORKER_STOP_POLL_IFACE _IOR(VHOST_POLLING_POLLWORKER_VIRTIO, 0x05, if_name_t)
#define VHOST_POLLING_POLLWORKER_BIND _IO(VHOST_POLLING_POLLWORKER_VIRTIO, 0x06)

#define VHOST_POLLING_POLLVQ_VIRTIO 0xC9
#define VHOST_POLLING_POLLVQ_GET_STATS _IOW(VHOST_POLLING_POLLVQ_VIRTIO, 0x01, struct vhost_pollvq_stats)
#define VHOST_POLLING_POLLVQ_SET_OWNER _IO(VHOST_POLLING_POLLVQ_VIRTIO, 0x02)
#define VHOST_POLLING_POLLVQ_UNSET_OWNER _IO(VHOST_POLLING_POLLVQ_VIRTIO, 0x03)
#define VHOST_POLLING_POLLVQ_ENABLE_SHARED _IO(VHOST_POLLING_POLLVQ_VIRTIO, 0x04)


struct vhost_pollvq_stats
{
    u64 devid;
    u64 vqid;
    u64 tsc; // time of get_stats ioctl

	u64 work_count; // Number of old-fashioned works done on vq

    u64 last_work; // Time of last work
    u64 work_time; // Total time we worked on the vq (only new way)
    u64 stuck_count; // Number of times the vq was stuck
    u64 limited_count; // Number of times vq was limited (because of max packets or because of a stuck queue)
    u64 stuck_time; // Total time the vq was stuck (only new way)
};

#endif
