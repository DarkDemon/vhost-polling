#ifndef I40E_POLL_H
#define I40E_POLL_H

#include <linux/netdevice.h>

extern int i40e_enable_poll(struct net_device * netdev);
extern int i40e_poll(struct net_device * netdev, int vector, int budget);
extern void i40e_disable_poll(struct net_device * netdev);

#endif /* I40E_POLL_H */